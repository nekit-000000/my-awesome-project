from collections import defaultdict
from task3 import load_graph


def dfs1(graph, start, t_in, t_out, visited=None, clock=0):
    if visited is None:
        visited = set()

    clock += 1
    t_in[start] = clock
    visited.add(start)

    if start in graph:
        for v in graph[start]:
            if v not in visited:
                clock = dfs1(graph, v, t_in, t_out, visited, clock)

    clock += 1
    t_out[start] = clock
    return clock


def dfs2(graph, start, visited, sc_component):
    visited.add(start)
    sc_component.append(start)
    if start in graph:
        for v in graph[start]:
            if v not in visited:
                dfs2(graph, v, visited, sc_component)


def invert_graph(graph):
    inverted_graph = defaultdict(list)
    for key in graph.keys():
        for value in graph[key]:
            inverted_graph[value].append(key)

    return inverted_graph


def find_sc_components(file_name):
    graph = load_graph(file_name)
    t_in = {}
    t_out = {}

    dfs1(graph, list(graph.keys())[0], t_in, t_out)
    time_sorted_vertices = sorted(
        t_out.keys(),
        key=lambda x: t_out[x],
        reverse=True)

    inverted_graph = invert_graph(graph)
    visited = set()
    sc_components = []
    for v in time_sorted_vertices:
        if v not in visited:
            sc_component = []
            dfs2(inverted_graph, v, visited, sc_component)
            sc_components.append("".join(sc_component))

    return sc_components


print(find_sc_components("TestData/SCComponents/test1.txt"))
