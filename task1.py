from lab_graph import LabyrinthGraph


def count_steps(graph, exit_vertex, scores):
    pos = exit_vertex
    cnt = 0
    while pos != graph.start_pos:
        cnt += 1
        for x in graph.adjacency_list[pos]:
            if scores[x] == scores[pos] - 1:
                pos = x
                break

    return cnt


def solve_lab_problem(file_name):
    graph = LabyrinthGraph.load_from_file(file_name)

    found_exit = False
    exit_vertex = ()
    scores = {key: -1 for key in graph.adjacency_list.keys()}
    scores[graph.start_pos] = 0

    for vertex in graph:
        if graph.is_exit(vertex):
            found_exit = True
            exit_vertex = vertex

        for x in graph.adjacency_list[vertex]:
            if scores[x] == -1:
                scores[x] = scores[vertex] + 1

    if not found_exit:
        return -1
    return count_steps(graph, exit_vertex, scores)
