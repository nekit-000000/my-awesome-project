parent = {}
rank = {}


def make_set(i):
    parent[i] = i
    rank[i] = 0


def find(i):
    if i != parent[i]:
        parent[i] = find(parent[i])
    return parent[i]


def union(i, j):
    i_id = find(i)
    j_id = find(j)
    if i_id == j_id:
        return
    if rank[i_id] > rank[j_id]:
        parent[j_id] = i_id
    else:
        parent[i_id] = j_id
    if rank[i_id] == rank[j_id]:
        rank[j_id] += 1


def load_edges(file_name):
    with open(file_name, 'r') as f:
        edges = []

        for line in f:
            data = line.split()
            edges.append((data[0], data[1], int(data[2])))

        return edges


def find_min_spanning_tree(file_name):
    edges = sorted(load_edges(file_name), key=lambda x: x[2])

    for edge in edges:
        make_set(edge[0])
        make_set(edge[1])

    spanning_tree_edges = []
    cost = 0
    for edge in edges:
        a, b, weight = edge
        if find(a) != find(b):
            cost += weight
            spanning_tree_edges.append((a, b))
            union(a, b)

    return spanning_tree_edges, cost
