def load_graph(file_name):
    with open(file_name, 'r') as f:
        graph = {}

        for line in f:
            vertices = line.split()
            graph[vertices[0]] = vertices[1:]

        return graph


def dfs(graph, start, t_in, t_out, unvisited, clock=0):
    clock += 1
    t_in[start] = clock

    if start in unvisited:
        unvisited.remove(start)

    if start in graph:
        for v in graph[start]:
            if v in unvisited:
                clock = dfs(graph, v, t_in, t_out, unvisited, clock)

    clock += 1
    t_out[start] = clock
    print("{}:{} {}".format(start, t_in[start], t_out[start]))
    return clock


def get_vertices(graph):
    vertices = set()
    for neighbours in graph.values():
        for v in neighbours:
            vertices.add(v)
    for v in graph.keys():
        vertices.add(v)

    return list(vertices)


def topological_sort(file_name):
    graph = load_graph(file_name)
    vertices = get_vertices(graph)

    unvisited = set(vertices)
    clock = 0
    t_in = {}
    t_out = {}
    while unvisited:
        clock = dfs(graph, unvisited.pop(), t_in, t_out, unvisited, clock)

    vertices.sort(key=lambda x: t_out[x], reverse=True)

    return vertices
