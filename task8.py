from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.base import BaseEstimator, ClassifierMixin
import numpy as np
import random


class MixedClassifier(BaseEstimator, ClassifierMixin):
    def __init__(self, alpha=0.0, c_log=1.0, c_svm=1.0):
        self.alpha = alpha
        self.c_log = c_log
        self.c_svm = c_svm
        self.svm_clf = None
        self.log_clf = None

    def fit(self, X, y=None):
        self.svm_clf = SVC(C=self.c_svm, probability=True, gamma="scale")
        self.log_clf = LogisticRegression(C=self.c_log, solver='lbfgs')
        self.svm_clf.fit(X, y)
        self.log_clf.fit(X, y)
        return self

    def predict(self, X, y=None):
        if self.svm_clf is None or self.log_clf is None:
            raise RuntimeError("You must train your classifier")

        c = self.svm_clf.classes_
        interpolated_probs = self.alpha * self.svm_clf.predict_proba(X) + \
            (1 - self.alpha) * self.log_clf.predict_proba(X)
        return np.array(
            [c[0] if p[0] > p[1] else c[1] for p in interpolated_probs]
        )


X = [[i] for i in range(0, 100, 5)]
y = [random.choice([0, 1]) for i in range(20)]
svm_c = 1.0
log_c = 1.0
alpha = 0.80

clf = MixedClassifier(alpha, log_c, svm_c)
clf.fit(X, y)

svm_clf = SVC(C=svm_c)
svm_clf.fit(X, y)

log_clf = LogisticRegression(C=log_c)
log_clf.fit(X, y)

tuned_params = {
    "alpha": [i for i in np.arange(0, 1.01, 0.01)],
    "c_log": [1],
    "c_svm": [1]
}

gs = GridSearchCV(MixedClassifier(), tuned_params)
gs.fit(X, y)

test_data = [[random.randint(0, 100)] for i in range(20)]
print(svm_clf.predict(test_data))
print(log_clf.predict(test_data))
print(clf.predict(test_data))
print(gs.predict(test_data))
