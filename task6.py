from collections import defaultdict
from priority_queue import  PriorityQueue
import math


def load_graph(file_name):
    with open(file_name, 'r') as f:
        graph = defaultdict(list)

        for line in f:
            data = line.split()
            graph[data[0]].append((data[1], int(data[2])))
            graph[data[1]].append((data[0], int(data[2])))

        return graph


def make_queue(graph, costs):
    queue = PriorityQueue(len(graph.keys()), lambda x, y: costs[x] < costs[y])
    for v in graph.keys():
        queue.insert(v)

    return queue


def find_min_spanning_tree_primm(file_name):
    graph = load_graph(file_name)

    costs = {}
    prev = {}
    for v in graph.keys():
        costs[v] = math.inf
        prev[v] = None

    costs[list(graph.keys())[0]] = 0
    queue = make_queue(graph, costs)

    while queue.size != 0:
        parent = queue.extract_min()
        for neighbour in graph[parent]:
            vertex, cost = neighbour
            if vertex in queue.H and costs[vertex] > cost:
                costs[vertex] = cost
                prev[vertex] = parent

                index = queue.H.index(vertex)
                queue.sift_down(index)
                queue.sift_up(index)

    return math.fsum(costs.values())
