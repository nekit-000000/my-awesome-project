﻿from matrix_graph import MatrixGraph


class LabyrinthGraph(MatrixGraph):
    def __init__(self, start_pos, matrix):
        self.start_pos = start_pos
        super().__init__(matrix)

    def __iter__(self):
        self.stack = [self.start_pos]
        self.unvisited_vertices = set(self.adjacency_list)
        self.unvisited_vertices.remove(self.start_pos)
        return self

    def __next__(self):
        if not self.stack and not self.unvisited_vertices:
            raise StopIteration

        if self.stack:
            pos = self.stack[-1]
        else:
            pos = self.unvisited_vertices.pop()

        self.stack.pop()
        neighbours = self.adjacency_list[pos]
        for x in neighbours:
            if x in self.unvisited_vertices:
                self.stack.append(x)
                self.unvisited_vertices.remove(x)
        return pos

    @staticmethod
    def load_from_file(file_name):
        with open(file_name, 'r') as f:
            start_pos = tuple(map(int, f.readline().split()))

            labyrinth = []
            for line in f:
                labyrinth.append([int(x) for x in line.split()])

            return LabyrinthGraph(start_pos, labyrinth)

    def is_exit(self, pos):
        return pos != self.start_pos and \
            (pos[0] == self.matrix_dim()[0] - 1 or
             pos[0] == 0 or
             pos[1] == self.matrix_dim()[1] - 1 or
             pos[1] == 0)
