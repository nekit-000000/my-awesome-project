class MatrixGraph:
    def __init__(self, matrix):
        self.matrix = matrix
        self.adjacency_list = {}
        self.island_cnt = 0

        for i, row in enumerate(matrix):
            for j in range(len(row)):
                if matrix[i][j] == 0:
                    continue
                pos = (j, i)
                self.adjacency_list[pos] = self.get_neighbours(pos)

    def matrix_dim(self):
        return len(self.matrix[0]), len(self.matrix)

    def in_map(self, pos):
        return 0 <= pos[0] < self.matrix_dim()[0] and \
               0 <= pos[1] < self.matrix_dim()[1]

    def _cast_to_valid_point(self, pos):
        if self.in_map(pos) and self.matrix[pos[1]][pos[0]] == 1:
            return pos
        else:
            return None

    def left(self, pos):
        return self._cast_to_valid_point((pos[0] - 1, pos[1]))

    def right(self, pos):
        return self._cast_to_valid_point((pos[0] + 1, pos[1]))

    def up(self, pos):
        return self._cast_to_valid_point((pos[0], pos[1] - 1))

    def down(self, pos):
        return self._cast_to_valid_point((pos[0], pos[1] + 1))

    def get_neighbours(self, pos):
        neighbours = [self.left(pos), self.right(pos),
                      self.up(pos), self.down(pos)]

        return list(filter(lambda x: x is not None, neighbours))
