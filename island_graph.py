from matrix_graph import MatrixGraph


class IslandGraph(MatrixGraph):
    def __iter__(self):
        keys_list = list(self.adjacency_list.keys())
        if not keys_list:
            self.stack = []
            self.unvisited_vertices = set()
            return self

        start_pos = list(self.adjacency_list.keys())[0]
        self.stack = [start_pos]
        self.unvisited_vertices = set(self.adjacency_list)
        self.unvisited_vertices.remove(start_pos)
        self.island_cnt = 1

        return self

    def __next__(self):
        if not self.stack and not self.unvisited_vertices:
            raise StopIteration

        if self.stack:
            pos = self.stack.pop()
        else:
            pos = self.unvisited_vertices.pop()
            self.island_cnt += 1

        neighbours = self.adjacency_list[pos]
        for x in neighbours:
            if x in self.unvisited_vertices:
                self.stack.append(x)
                self.unvisited_vertices.remove(x)
        return pos

    @staticmethod
    def load_from_file(file_name):
        with open(file_name, 'r') as f:
            matrix = []
            for line in f:
                matrix.append(list(map(int, line.split())))

            return IslandGraph(matrix)

    def count_num_of_islands(self):
        for _ in self:
            pass
        return self.island_cnt
