class PriorityQueue:
    def __init__(self, max_size, compare_func=lambda x, y: x < y):
        self.H = [0 for _ in range(max_size + 1)]
        self.max_size = max_size
        self.compare_func = compare_func
        self.size = 0

    def sift_up(self, i):
        while i > 1 and not self.compare_func(self.H[parent(i)], self.H[i]):
            p = parent(i)
            self.H[p], self.H[i] = self.H[i], self.H[p]
            i = p

    def sift_down(self, i):
        min_index = i
        l = left(i)
        r = right(i)

        if l <= self.size and self.compare_func(self.H[l], self.H[min_index]):
            min_index = l

        if r <= self.size and self.compare_func(self.H[r], self.H[min_index]):
            min_index = r

        if i != min_index:
            self.H[min_index], self.H[i] = self.H[i], self.H[min_index]
            self.sift_down(min_index)

    def insert(self, p):
        if self.size == self.max_size:
            self.max_size *= 2
            self.H = [self.H[i] if i <= self.size else 0
                      for i in range(self.max_size + 1)]

        self.size += 1
        self.H[self.size] = p
        self.sift_up(self.size)

    def extract_min(self):
        result = self.H[1]
        self.H[1] = self.H[self.size]
        self.H[self.size] = 0
        self.size -= 1
        self.sift_down(1)

        return result

    def change_priority(self, i, p):
        old_p = self.H[i]
        self.H[i] = p

        if self.compare_func(p, old_p):
            self.sift_down(i)
        else:
            self.sift_up(i)


def parent(i):
    return int(i / 2)


def left(i):
    return 2 * i


def right(i):
    return 2 * i + 1
