﻿import unittest
import inspect
from task1 import solve_lab_problem
from task2 import solve_island_problem
from task4 import find_sc_components
from task5 import find_min_spanning_tree
from task6 import find_min_spanning_tree_primm
from task9 import is_valid_search_tree
from priority_queue import PriorityQueue


class ProblemTestCase(unittest.TestCase):
    def setUp(self):
        self.folder = None
        self.solver = None

    def do_test(self):
        test_name = inspect.currentframe().f_back.f_code.co_name

        ans_file_name = "TestData/{}/ans_{}.txt".format(self.folder, test_name)
        with open(ans_file_name, 'r') as f_ans:
            ans = int(f_ans.readline())
            test_file_name = \
                "TestData/{}/{}.txt".format(self.folder, test_name)
            self.assertEqual(
                ans,
                self.solver(test_file_name)
            )


class Task1Tests(ProblemTestCase):
    def setUp(self):
        self.folder = "Labyrinth"
        self.solver = solve_lab_problem

    def test1(self):
        self.do_test()

    def test2(self):
        self.do_test()

    def test3(self):
        self.do_test()


class Task2Tests(ProblemTestCase):
    def setUp(self):
        self.folder = "Island"
        self.solver = solve_island_problem

    def test1(self):
        self.do_test()

    def test2(self):
        self.do_test()

    def test3(self):
        self.do_test()


class Task4Tests(ProblemTestCase):
    def setUp(self):
        self.folder = "SCComponents"
        self.solver = find_sc_components

    def do_test(self):
        test_name = inspect.currentframe().f_back.f_code.co_name

        ans_file_name = "TestData/{}/ans_{}.txt".format(self.folder, test_name)
        with open(ans_file_name, 'r') as f_ans:
            ans = f_ans.readline().split()
            test_file_name = \
                "TestData/{}/{}.txt".format(self.folder, test_name)

            solution = self.solver(test_file_name)
            for v in ans:
                self.assertTrue(v in solution)

    def test1(self):
        self.do_test()


class Task5Tests(ProblemTestCase):
    def setUp(self):
        self.folder = "MinSpanningTree"
        self.solver = find_min_spanning_tree

    def do_test(self):
        test_name = inspect.currentframe().f_back.f_code.co_name

        ans_file_name = "TestData/{}/ans_{}.txt".format(self.folder, test_name)
        with open(ans_file_name, 'r') as f_ans:
            edge_count = int(f_ans.readline())
            ans = []
            for i in range(edge_count):
                ans.append(tuple(f_ans.readline().split()))
            weight = int(f_ans.readline())

            test_file_name = \
                "TestData/{}/{}.txt".format(self.folder, test_name)

            solution = self.solver(test_file_name)
            for v in ans:
                self.assertTrue(v in solution[0])
            self.assertEqual(solution[1], weight)

    def test1(self):
        self.do_test()


class Task6Tests(ProblemTestCase):
    def setUp(self):
        self.folder = "MinSpanningTreePrimm"
        self.solver = find_min_spanning_tree_primm

    def test1(self):
        self.do_test()


class PriorityQueueTest(unittest.TestCase):
    def testMultiInsert(self):
        x = range(100)
        q = PriorityQueue(1)

        for i in x:
            q.insert(i)

        for i in x:
            self.assertEqual(i, q.extract_min())


class Task9Tests(ProblemTestCase):
    def setUp(self):
        self.folder = "Tree"
        self.solver = find_min_spanning_tree

    def do_test(self):
        test_name = inspect.currentframe().f_back.f_code.co_name

        ans_file_name = "TestData/{}/ans_{}.txt".format(self.folder, test_name)
        with open(ans_file_name, 'r') as f_ans:
            test_file_name = \
                "TestData/{}/{}.txt".format(self.folder, test_name)

            ans = True if f_ans.readline()[0] == 'T' else False
            self.assertEqual(is_valid_search_tree(test_file_name), ans)

    def test1(self):
        self.do_test()

    def test2(self):
        self.do_test()


if __name__ == '__main__':
    unittest.main()
