from island_graph import IslandGraph


def solve_island_problem(file_name):
    return IslandGraph.load_from_file(file_name).count_num_of_islands()
