class Node:
    def __init__(self, value):
        self.left = None
        self.right = None
        self.data = value


def load_tree(file_name):
    def construct_tree(node, index):
        left_ind = 2 * index
        right_ind = left_ind + 1

        if left_ind >= len(nodes):
            return

        node.left = Node(nodes[left_ind])
        node.right = Node(nodes[right_ind])
        construct_tree(node.left, left_ind)
        construct_tree(node.right, right_ind)

    with open(file_name, 'r') as f:
        nodes = [0]
        nodes.extend(list(map(int, f.readline().split())))

        root = Node(nodes[1])
        construct_tree(root, 1)
        return root


def find_value_by_func(start, func, first_value):
    queue = [start]
    value = first_value

    while queue:
        vertex = queue.pop(0)

        if func(vertex.data, value):
            value = vertex.data

        queue.append(vertex.left)
        queue.append(vertex.right)

    return value


def dfs(start, max_vals, min_vals):
    if start.left is None:
        min_vals[start] = start.data
    else:
        if not dfs(start.left, max_vals, min_vals):
            return False
        min_vals[start] = min_vals[start.left]

        if max_vals[start.left] > start.data:
            return False

    if start.right is None:
        max_vals[start] = start.data
    else:
        if not dfs(start.right, max_vals, min_vals):
            return False
        max_vals[start] = max_vals[start.right]

        if min_vals[start.right] < start.data:
            return False

    return True


def check_left_right(start):
    if start is None:
        return True
    if start.left is not None and start.left.data >= start.data:
        return False
    if start.right is not None and start.right.data <= start.data:
        return False

    return check_left_right(start.left) and \
        check_left_right(start.right)


def is_valid_search_tree(file_name):
    root = load_tree(file_name)

    if not check_left_right(root):
        return False

    min_vals = {}
    max_vals = {}
    return dfs(root, max_vals, min_vals)
